-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 14 Paź 2018, 22:34
-- Wersja serwera: 5.7.11
-- Wersja PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `lexus`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lx_bed`
--

CREATE TABLE `lx_bed` (
  `id` int(11) NOT NULL,
  `bed_number` int(11) NOT NULL,
  `day` date NOT NULL,
  `room` int(11) NOT NULL,
  `reserved` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `lx_bed`
--

INSERT INTO `lx_bed` (`id`, `bed_number`, `day`, `room`, `reserved`) VALUES
(81, 1, '2018-11-21', 1, NULL),
(82, 1, '2018-11-22', 1, NULL),
(83, 2, '2018-11-21', 1, NULL),
(84, 2, '2018-11-22', 1, NULL),
(85, 3, '2018-11-21', 1, NULL),
(86, 3, '2018-11-22', 1, NULL),
(87, 4, '2018-11-21', 1, NULL),
(88, 4, '2018-11-22', 1, NULL),
(89, 5, '2018-11-21', 1, NULL),
(90, 5, '2018-11-22', 1, NULL),
(91, 6, '2018-11-21', 1, NULL),
(92, 6, '2018-11-22', 1, NULL),
(93, 1, '2018-11-21', 2, NULL),
(94, 1, '2018-11-22', 2, NULL),
(95, 2, '2018-11-21', 2, NULL),
(96, 2, '2018-11-22', 2, NULL),
(97, 3, '2018-11-21', 2, NULL),
(98, 3, '2018-11-22', 2, NULL),
(99, 4, '2018-11-21', 2, NULL),
(100, 4, '2018-11-22', 2, NULL),
(101, 1, '2018-11-21', 3, NULL),
(102, 1, '2018-11-22', 3, NULL),
(103, 2, '2018-11-21', 3, NULL),
(104, 2, '2018-11-22', 3, NULL),
(105, 3, '2018-11-21', 3, NULL),
(106, 3, '2018-11-22', 3, NULL),
(107, 4, '2018-11-21', 3, NULL),
(108, 4, '2018-11-22', 3, NULL),
(109, 1, '2018-11-21', 4, NULL),
(110, 1, '2018-11-22', 4, NULL),
(111, 2, '2018-11-21', 4, NULL),
(112, 2, '2018-11-22', 4, NULL),
(113, 3, '2018-11-21', 4, NULL),
(114, 3, '2018-11-22', 4, NULL),
(115, 4, '2018-11-21', 4, NULL),
(116, 4, '2018-11-22', 4, NULL),
(117, 1, '2018-11-21', 5, NULL),
(118, 1, '2018-11-22', 5, NULL),
(119, 2, '2018-11-21', 5, NULL),
(120, 2, '2018-11-22', 5, NULL),
(121, 3, '2018-11-21', 5, NULL),
(122, 3, '2018-11-22', 5, NULL),
(123, 4, '2018-11-21', 5, NULL),
(124, 4, '2018-11-22', 5, NULL),
(125, 1, '2018-11-21', 6, NULL),
(126, 1, '2018-11-22', 6, NULL),
(127, 2, '2018-11-21', 6, NULL),
(128, 2, '2018-11-22', 6, NULL),
(129, 1, '2018-11-21', 7, NULL),
(130, 1, '2018-11-22', 7, NULL),
(131, 2, '2018-11-21', 7, NULL),
(132, 2, '2018-11-22', 7, NULL),
(133, 3, '2018-11-21', 7, NULL),
(134, 3, '2018-11-22', 7, NULL),
(135, 1, '2018-11-21', 8, NULL),
(136, 1, '2018-11-22', 8, NULL),
(137, 2, '2018-11-21', 8, NULL),
(138, 2, '2018-11-22', 8, NULL),
(139, 3, '2018-11-21', 8, NULL),
(140, 3, '2018-11-22', 8, NULL),
(141, 4, '2018-11-21', 8, NULL),
(142, 4, '2018-11-22', 8, NULL),
(143, 1, '2018-11-21', 9, NULL),
(144, 1, '2018-11-22', 9, NULL),
(145, 2, '2018-11-21', 9, NULL),
(146, 2, '2018-11-22', 9, NULL),
(147, 3, '2018-11-21', 9, NULL),
(148, 3, '2018-11-22', 9, NULL),
(149, 4, '2018-11-21', 9, NULL),
(150, 4, '2018-11-22', 9, NULL),
(151, 1, '2018-11-21', 10, NULL),
(152, 1, '2018-11-22', 10, NULL),
(153, 2, '2018-11-21', 10, NULL),
(154, 2, '2018-11-22', 10, NULL),
(155, 3, '2018-11-21', 10, NULL),
(156, 3, '2018-11-22', 10, NULL),
(157, 4, '2018-11-21', 10, NULL),
(158, 4, '2018-11-22', 10, NULL),
(159, 1, '2018-11-21', 11, NULL),
(160, 1, '2018-11-22', 11, NULL),
(161, 2, '2018-11-21', 11, NULL),
(162, 2, '2018-11-22', 11, NULL),
(163, 3, '2018-11-21', 11, NULL),
(164, 3, '2018-11-22', 11, NULL),
(165, 1, '2018-11-21', 12, NULL),
(166, 1, '2018-11-22', 12, NULL),
(167, 2, '2018-11-21', 12, NULL),
(168, 2, '2018-11-22', 12, NULL),
(169, 3, '2018-11-21', 12, NULL),
(170, 3, '2018-11-22', 12, NULL),
(171, 4, '2018-11-21', 12, NULL),
(172, 4, '2018-11-22', 12, NULL),
(173, 1, '2018-11-21', 13, NULL),
(174, 1, '2018-11-22', 13, NULL),
(175, 2, '2018-11-21', 13, NULL),
(176, 2, '2018-11-22', 13, NULL),
(177, 1, '2018-11-21', 14, NULL),
(178, 1, '2018-11-22', 14, NULL),
(179, 2, '2018-11-21', 14, NULL),
(180, 2, '2018-11-22', 14, NULL),
(181, 3, '2018-11-21', 14, NULL),
(182, 3, '2018-11-22', 14, NULL),
(183, 4, '2018-11-21', 14, NULL),
(184, 4, '2018-11-22', 14, NULL),
(185, 1, '2018-11-21', 15, NULL),
(186, 1, '2018-11-22', 15, NULL),
(187, 2, '2018-11-21', 15, NULL),
(188, 2, '2018-11-22', 15, NULL),
(189, 1, '2018-11-21', 16, NULL),
(190, 1, '2018-11-22', 16, NULL),
(191, 1, '2018-11-21', 17, NULL),
(192, 1, '2018-11-22', 17, NULL),
(193, 2, '2018-11-21', 17, NULL),
(194, 2, '2018-11-22', 17, NULL),
(195, 3, '2018-11-21', 17, NULL),
(196, 3, '2018-11-22', 17, NULL),
(197, 4, '2018-11-21', 17, NULL),
(198, 4, '2018-11-22', 17, NULL),
(199, 1, '2018-11-21', 18, NULL),
(200, 1, '2018-11-22', 18, NULL),
(201, 2, '2018-11-21', 18, NULL),
(202, 2, '2018-11-22', 18, NULL),
(203, 3, '2018-11-21', 18, NULL),
(204, 3, '2018-11-22', 18, NULL),
(205, 4, '2018-11-21', 18, NULL),
(206, 4, '2018-11-22', 18, NULL),
(207, 1, '2018-11-21', 19, NULL),
(208, 1, '2018-11-22', 19, NULL),
(209, 2, '2018-11-21', 19, NULL),
(210, 2, '2018-11-22', 19, NULL),
(211, 3, '2018-11-21', 19, NULL),
(212, 3, '2018-11-22', 19, NULL),
(213, 4, '2018-11-21', 19, NULL),
(214, 4, '2018-11-22', 19, NULL),
(215, 1, '2018-11-21', 25, NULL),
(216, 1, '2018-11-22', 25, NULL),
(217, 2, '2018-11-21', 25, NULL),
(218, 2, '2018-11-22', 25, NULL),
(219, 3, '2018-11-21', 25, NULL),
(220, 3, '2018-11-22', 25, NULL),
(221, 4, '2018-11-21', 25, NULL),
(222, 4, '2018-11-22', 25, NULL),
(223, 1, '2018-11-21', 26, NULL),
(224, 1, '2018-11-22', 26, NULL),
(225, 2, '2018-11-21', 26, NULL),
(226, 2, '2018-11-22', 26, NULL),
(227, 3, '2018-11-21', 26, NULL),
(228, 3, '2018-11-22', 26, NULL),
(229, 4, '2018-11-21', 26, NULL),
(230, 4, '2018-11-22', 26, NULL),
(231, 5, '2018-11-21', 26, NULL),
(232, 5, '2018-11-22', 26, NULL),
(233, 6, '2018-11-21', 26, NULL),
(234, 6, '2018-11-22', 26, NULL),
(235, 1, '2018-11-21', 27, NULL),
(236, 1, '2018-11-22', 27, NULL),
(237, 2, '2018-11-21', 27, NULL),
(238, 2, '2018-11-22', 27, NULL),
(239, 3, '2018-11-21', 27, NULL),
(240, 3, '2018-11-22', 27, NULL),
(241, 4, '2018-11-21', 27, NULL),
(242, 4, '2018-11-22', 27, NULL),
(243, 5, '2018-11-21', 27, NULL),
(244, 5, '2018-11-22', 27, NULL),
(245, 6, '2018-11-21', 27, NULL),
(246, 6, '2018-11-22', 27, NULL),
(247, 1, '2018-11-21', 28, NULL),
(248, 1, '2018-11-22', 28, NULL),
(249, 2, '2018-11-21', 28, NULL),
(250, 2, '2018-11-22', 28, NULL),
(251, 3, '2018-11-21', 28, NULL),
(252, 3, '2018-11-22', 28, NULL),
(253, 4, '2018-11-21', 28, NULL),
(254, 4, '2018-11-22', 28, NULL),
(255, 5, '2018-11-21', 28, NULL),
(256, 5, '2018-11-22', 28, NULL),
(257, 6, '2018-11-21', 28, NULL),
(258, 6, '2018-11-22', 28, NULL),
(259, 1, '2018-11-21', 29, NULL),
(260, 1, '2018-11-22', 29, NULL),
(261, 2, '2018-11-21', 29, NULL),
(262, 2, '2018-11-22', 29, NULL),
(263, 3, '2018-11-21', 29, NULL),
(264, 3, '2018-11-22', 29, NULL),
(265, 4, '2018-11-21', 29, NULL),
(266, 4, '2018-11-22', 29, NULL),
(267, 1, '2018-11-21', 30, NULL),
(268, 1, '2018-11-22', 30, NULL),
(269, 2, '2018-11-21', 30, NULL),
(270, 2, '2018-11-22', 30, NULL),
(271, 3, '2018-11-21', 30, NULL),
(272, 3, '2018-11-22', 30, NULL),
(273, 4, '2018-11-21', 30, NULL),
(274, 4, '2018-11-22', 30, NULL),
(275, 1, '2018-11-21', 31, NULL),
(276, 1, '2018-11-22', 31, NULL),
(277, 2, '2018-11-21', 31, NULL),
(278, 2, '2018-11-22', 31, NULL),
(279, 3, '2018-11-21', 31, NULL),
(280, 3, '2018-11-22', 31, NULL),
(281, 1, '2018-11-21', 32, NULL),
(282, 1, '2018-11-22', 32, NULL),
(283, 2, '2018-11-21', 32, NULL),
(284, 2, '2018-11-22', 32, NULL),
(285, 3, '2018-11-21', 32, NULL),
(286, 3, '2018-11-22', 32, NULL),
(287, 4, '2018-11-21', 32, NULL),
(288, 4, '2018-11-22', 32, NULL),
(289, 1, '2018-11-21', 33, NULL),
(290, 1, '2018-11-22', 33, NULL),
(291, 2, '2018-11-21', 33, NULL),
(292, 2, '2018-11-22', 33, NULL),
(293, 3, '2018-11-21', 33, NULL),
(294, 3, '2018-11-22', 33, NULL),
(295, 4, '2018-11-21', 33, NULL),
(296, 4, '2018-11-22', 33, NULL),
(297, 1, '2018-11-21', 34, NULL),
(298, 1, '2018-11-22', 34, NULL),
(299, 2, '2018-11-21', 34, NULL),
(300, 2, '2018-11-22', 34, NULL),
(301, 3, '2018-11-21', 34, NULL),
(302, 3, '2018-11-22', 34, NULL),
(303, 4, '2018-11-21', 34, NULL),
(304, 4, '2018-11-22', 34, NULL),
(305, 1, '2018-11-21', 35, NULL),
(306, 1, '2018-11-22', 35, NULL),
(307, 2, '2018-11-21', 35, NULL),
(308, 2, '2018-11-22', 35, NULL),
(309, 3, '2018-11-21', 35, NULL),
(310, 3, '2018-11-22', 35, NULL),
(311, 4, '2018-11-21', 35, NULL),
(312, 4, '2018-11-22', 35, NULL),
(313, 1, '2018-11-21', 36, NULL),
(314, 1, '2018-11-22', 36, NULL),
(315, 2, '2018-11-21', 36, NULL),
(316, 2, '2018-11-22', 36, NULL),
(317, 3, '2018-11-21', 36, NULL),
(318, 3, '2018-11-22', 36, NULL),
(319, 4, '2018-11-21', 36, NULL),
(320, 4, '2018-11-22', 36, NULL),
(321, 1, '2018-11-21', 37, NULL),
(322, 1, '2018-11-22', 37, NULL),
(323, 2, '2018-11-21', 37, NULL),
(324, 2, '2018-11-22', 37, NULL),
(325, 3, '2018-11-21', 37, NULL),
(326, 3, '2018-11-22', 37, NULL),
(327, 1, '2018-11-21', 38, NULL),
(328, 1, '2018-11-22', 38, NULL),
(329, 2, '2018-11-21', 38, NULL),
(330, 2, '2018-11-22', 38, NULL),
(331, 3, '2018-11-21', 38, NULL),
(332, 3, '2018-11-22', 38, NULL),
(333, 4, '2018-11-21', 38, NULL),
(334, 4, '2018-11-22', 38, NULL),
(335, 1, '2018-11-21', 39, NULL),
(336, 1, '2018-11-22', 39, NULL),
(337, 2, '2018-11-21', 39, NULL),
(338, 2, '2018-11-22', 39, NULL),
(339, 3, '2018-11-21', 39, NULL),
(340, 3, '2018-11-22', 39, NULL),
(341, 1, '2018-11-21', 40, NULL),
(342, 1, '2018-11-22', 40, NULL),
(343, 2, '2018-11-21', 40, NULL),
(344, 2, '2018-11-22', 40, NULL),
(345, 3, '2018-11-21', 40, NULL),
(346, 3, '2018-11-22', 40, NULL),
(347, 4, '2018-11-21', 40, NULL),
(348, 4, '2018-11-22', 40, NULL),
(349, 5, '2018-11-21', 40, NULL),
(350, 5, '2018-11-22', 40, NULL),
(351, 1, '2018-11-21', 41, NULL),
(352, 1, '2018-11-22', 41, NULL),
(353, 2, '2018-11-21', 41, NULL),
(354, 2, '2018-11-22', 41, NULL),
(355, 1, '2018-11-21', 42, NULL),
(356, 1, '2018-11-22', 42, NULL),
(357, 2, '2018-11-21', 42, NULL),
(358, 2, '2018-11-22', 42, NULL),
(359, 3, '2018-11-21', 42, NULL),
(360, 3, '2018-11-22', 42, NULL),
(361, 4, '2018-11-21', 42, NULL),
(362, 4, '2018-11-22', 42, NULL),
(363, 1, '2018-11-21', 43, NULL),
(364, 1, '2018-11-22', 43, NULL),
(365, 2, '2018-11-21', 43, NULL),
(366, 2, '2018-11-22', 43, NULL),
(367, 3, '2018-11-21', 43, NULL),
(368, 3, '2018-11-22', 43, NULL),
(369, 1, '2018-11-21', 44, NULL),
(370, 1, '2018-11-22', 44, NULL),
(371, 2, '2018-11-21', 44, NULL),
(372, 2, '2018-11-22', 44, NULL),
(373, 3, '2018-11-21', 44, NULL),
(374, 3, '2018-11-22', 44, NULL),
(375, 4, '2018-11-21', 44, NULL),
(376, 4, '2018-11-22', 44, NULL),
(377, 1, '2018-11-21', 45, NULL),
(378, 1, '2018-11-22', 45, NULL),
(379, 2, '2018-11-21', 45, NULL),
(380, 2, '2018-11-22', 45, NULL),
(381, 3, '2018-11-21', 45, NULL),
(382, 3, '2018-11-22', 45, NULL),
(383, 4, '2018-11-21', 45, NULL),
(384, 4, '2018-11-22', 45, NULL),
(385, 5, '2018-11-21', 45, NULL),
(386, 5, '2018-11-22', 45, NULL),
(387, 6, '2018-11-21', 45, NULL),
(388, 6, '2018-11-22', 45, NULL),
(389, 1, '2018-11-21', 46, NULL),
(390, 1, '2018-11-22', 46, NULL),
(391, 2, '2018-11-21', 46, NULL),
(392, 2, '2018-11-22', 46, NULL),
(393, 3, '2018-11-21', 46, NULL),
(394, 3, '2018-11-22', 46, NULL),
(395, 4, '2018-11-21', 46, NULL),
(396, 4, '2018-11-22', 46, NULL),
(397, 5, '2018-11-21', 46, NULL),
(398, 5, '2018-11-22', 46, NULL),
(399, 1, '2018-11-21', 47, NULL),
(400, 1, '2018-11-22', 47, NULL),
(401, 2, '2018-11-21', 47, NULL),
(402, 2, '2018-11-22', 47, NULL),
(403, 3, '2018-11-21', 47, NULL),
(404, 3, '2018-11-22', 47, NULL),
(405, 4, '2018-11-21', 47, NULL),
(406, 4, '2018-11-22', 47, NULL),
(407, 1, '2018-11-21', 48, NULL),
(408, 1, '2018-11-22', 48, NULL),
(409, 2, '2018-11-21', 48, NULL),
(410, 2, '2018-11-22', 48, NULL),
(411, 3, '2018-11-21', 48, NULL),
(412, 3, '2018-11-22', 48, NULL),
(413, 4, '2018-11-21', 48, NULL),
(414, 4, '2018-11-22', 48, NULL),
(415, 1, '2018-11-21', 49, NULL),
(416, 1, '2018-11-22', 49, NULL),
(417, 2, '2018-11-21', 49, NULL),
(418, 2, '2018-11-22', 49, NULL),
(419, 3, '2018-11-21', 49, NULL),
(420, 3, '2018-11-22', 49, NULL),
(421, 4, '2018-11-21', 49, NULL),
(422, 4, '2018-11-22', 49, NULL),
(423, 1, '2018-11-21', 50, NULL),
(424, 1, '2018-11-22', 50, NULL),
(425, 2, '2018-11-21', 50, NULL),
(426, 2, '2018-11-22', 50, NULL),
(427, 3, '2018-11-21', 50, NULL),
(428, 3, '2018-11-22', 50, NULL),
(429, 4, '2018-11-21', 50, NULL),
(430, 4, '2018-11-22', 50, NULL),
(431, 1, '2018-11-21', 51, NULL),
(432, 1, '2018-11-22', 51, NULL),
(433, 2, '2018-11-21', 51, NULL),
(434, 2, '2018-11-22', 51, NULL),
(435, 3, '2018-11-21', 51, NULL),
(436, 3, '2018-11-22', 51, NULL),
(437, 4, '2018-11-21', 51, NULL),
(438, 4, '2018-11-22', 51, NULL),
(439, 1, '2018-11-21', 52, NULL),
(440, 1, '2018-11-22', 52, NULL),
(441, 2, '2018-11-21', 52, NULL),
(442, 2, '2018-11-22', 52, NULL),
(443, 3, '2018-11-21', 52, NULL),
(444, 3, '2018-11-22', 52, NULL),
(445, 4, '2018-11-21', 52, NULL),
(446, 4, '2018-11-22', 52, NULL),
(447, 1, '2018-11-21', 53, NULL),
(448, 1, '2018-11-22', 53, NULL),
(449, 2, '2018-11-21', 53, NULL),
(450, 2, '2018-11-22', 53, NULL),
(451, 3, '2018-11-21', 53, NULL),
(452, 3, '2018-11-22', 53, NULL),
(453, 4, '2018-11-21', 53, NULL),
(454, 4, '2018-11-22', 53, NULL),
(455, 1, '2018-11-21', 54, NULL),
(456, 1, '2018-11-22', 54, NULL),
(457, 1, '2018-11-21', 55, NULL),
(458, 1, '2018-11-22', 55, NULL),
(459, 2, '2018-11-21', 55, NULL),
(460, 2, '2018-11-22', 55, NULL),
(461, 1, '2018-11-21', 56, NULL),
(462, 1, '2018-11-22', 56, NULL),
(463, 2, '2018-11-21', 56, NULL),
(464, 2, '2018-11-22', 56, NULL),
(465, 3, '2018-11-21', 56, NULL),
(466, 3, '2018-11-22', 56, NULL),
(467, 1, '2018-11-21', 57, NULL),
(468, 1, '2018-11-22', 57, NULL),
(469, 2, '2018-11-21', 57, NULL),
(470, 2, '2018-11-22', 57, NULL),
(471, 3, '2018-11-21', 57, NULL),
(472, 3, '2018-11-22', 57, NULL),
(473, 4, '2018-11-21', 57, NULL),
(474, 4, '2018-11-22', 57, NULL),
(475, 1, '2018-11-21', 58, NULL),
(476, 1, '2018-11-22', 58, NULL),
(477, 2, '2018-11-21', 58, NULL),
(478, 2, '2018-11-22', 58, NULL),
(479, 3, '2018-11-21', 58, NULL),
(480, 3, '2018-11-22', 58, NULL),
(481, 4, '2018-11-21', 58, NULL),
(482, 4, '2018-11-22', 58, NULL),
(483, 1, '2018-11-21', 59, NULL),
(484, 1, '2018-11-22', 59, NULL),
(485, 2, '2018-11-21', 59, NULL),
(486, 2, '2018-11-22', 59, NULL),
(487, 3, '2018-11-21', 59, NULL),
(488, 3, '2018-11-22', 59, NULL),
(489, 1, '2018-11-21', 60, NULL),
(490, 1, '2018-11-22', 60, NULL),
(491, 2, '2018-11-21', 60, NULL),
(492, 2, '2018-11-22', 60, NULL),
(493, 3, '2018-11-21', 60, NULL),
(494, 3, '2018-11-22', 60, NULL),
(495, 1, '2018-11-21', 61, NULL),
(496, 1, '2018-11-22', 61, NULL),
(497, 2, '2018-11-21', 61, NULL),
(498, 2, '2018-11-22', 61, NULL),
(499, 3, '2018-11-21', 61, NULL),
(500, 3, '2018-11-22', 61, NULL),
(501, 1, '2018-11-21', 62, NULL),
(502, 1, '2018-11-22', 62, NULL),
(503, 2, '2018-11-21', 62, NULL),
(504, 2, '2018-11-22', 62, NULL),
(505, 3, '2018-11-21', 62, NULL),
(506, 3, '2018-11-22', 62, NULL),
(507, 1, '2018-11-21', 63, NULL),
(508, 1, '2018-11-22', 63, NULL),
(509, 2, '2018-11-21', 63, NULL),
(510, 2, '2018-11-22', 63, NULL),
(511, 3, '2018-11-21', 63, NULL),
(512, 3, '2018-11-22', 63, NULL),
(513, 4, '2018-11-21', 63, NULL),
(514, 4, '2018-11-22', 63, NULL),
(515, 5, '2018-11-21', 63, NULL),
(516, 5, '2018-11-22', 63, NULL),
(517, 6, '2018-11-21', 63, NULL),
(518, 6, '2018-11-22', 63, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lx_order`
--

CREATE TABLE `lx_order` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci,
  `short_name` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `to_pay` int(11) DEFAULT NULL,
  `paid` int(11) DEFAULT NULL,
  `status` varchar(127) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lx_order_item`
--

CREATE TABLE `lx_order_item` (
  `id` int(11) NOT NULL,
  `oorder` int(11) NOT NULL,
  `bed` int(11) NOT NULL,
  `vegetarians` tinyint(1) DEFAULT NULL,
  `person_name` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lx_room`
--

CREATE TABLE `lx_room` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `lx_room`
--

INSERT INTO `lx_room` (`id`, `name`) VALUES
(1, 'Pokój 1 zajazd'),
(2, 'Pokój 2 zajazd'),
(3, 'Pokój 3 zajazd'),
(4, 'Pokój 4 zajazd'),
(5, 'Pokój 5 zajazd'),
(6, 'Pokój 6 zajazd'),
(7, 'Pokój 7 zajazd'),
(8, 'Pokój 8 zajazd'),
(9, 'Pokój 9 zajazd'),
(10, 'Pokój 10 zajazd'),
(11, 'Pokój 11 zajazd'),
(12, 'Pokój 12 zajazd'),
(13, 'Pokój 13 zajazd'),
(14, 'Pokój 14 zajazd'),
(15, 'Pokój 15 zajazd'),
(16, 'Pokój 16 zajazd'),
(17, 'Pokój 17 zajazd'),
(18, 'Pokój 18 zajazd'),
(19, 'Pokój 19 zajazd'),
(25, 'Pokój 25 zajazd'),
(26, 'Pokój 26 zajazd'),
(27, 'Pokój 27 zajazd'),
(28, 'Pokój 28 zajazd'),
(29, 'Pokój 29 zajazd'),
(30, 'Pokój 30 zajazd'),
(31, 'Pokój 31 zajazd'),
(32, 'Pokój 32 zajazd'),
(33, 'Pokój 33 zajazd'),
(34, 'Pokój 34 zajazd'),
(35, 'Pokój 35 zajazd'),
(36, 'Pokój 36 zajazd'),
(37, 'Pokój 37 zajazd'),
(38, 'Pokój 38 zajazd'),
(39, 'Pokój 39 zajazd'),
(40, 'Pokój 40 zajazd'),
(41, 'Pokój 0 rancho'),
(42, 'Pokój 1 rancho'),
(43, 'Pokój 2 rancho'),
(44, 'Pokój 3 rancho'),
(45, 'Pokój 4 rancho'),
(46, 'Pokój 5 rancho'),
(47, 'Pokój 6 rancho'),
(48, 'Pokój 7 rancho'),
(49, 'Pokój 8 rancho'),
(50, 'Pokój 9 rancho'),
(51, 'Pokój 10 rancho'),
(52, 'Pokój 11 rancho'),
(53, 'Pokój 12 rancho'),
(54, 'Pokój 13 rancho'),
(55, 'Pokój 14 rancho'),
(56, 'Pokój 15 rancho'),
(57, 'Pokój 16 rancho'),
(58, 'Pokój 17 rancho'),
(59, 'Pokój 18 rancho'),
(60, 'Pokój 19 rancho'),
(61, 'Pokój 20 rancho'),
(62, 'Pokój 21 rancho'),
(63, 'Pokój 22 rancho');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `account_non_expired` tinyint(1) NOT NULL,
  `account_non_locked` tinyint(1) NOT NULL,
  `credentials_non_expired` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `action_token` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `register_date` datetime NOT NULL,
  `avatar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `account_non_expired`, `account_non_locked`, `credentials_non_expired`, `enabled`, `roles`, `action_token`, `register_date`, `avatar`, `update_date`) VALUES
(11, '555', 'docent@sdg.pl', '$2y$13$EPvEzKCUYDb0fnwcF3ikOeJl.oqjC.zZXi4Xb3Nyb08eCJPzh99Dq', 1, 1, 1, 1, 'a:2:{i:0;s:9:"ROLE_USER";i:1;s:10:"ROLE_ADMIN";}', 'bf1eac1a225a16d1cb0b', '2018-10-14 21:16:16', NULL, NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `lx_bed`
--
ALTER TABLE `lx_bed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8D3EFF95729F519B` (`room`);

--
-- Indexes for table `lx_order`
--
ALTER TABLE `lx_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F3322A768D93D649` (`user`);

--
-- Indexes for table `lx_order_item`
--
ALTER TABLE `lx_order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E35D8CB07B6B78A9` (`oorder`),
  ADD KEY `IDX_E35D8CB0E647FCFF` (`bed`);

--
-- Indexes for table `lx_room`
--
ALTER TABLE `lx_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1483A5E9F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_1483A5E9E7927C74` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `lx_bed`
--
ALTER TABLE `lx_bed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=519;
--
-- AUTO_INCREMENT dla tabeli `lx_order`
--
ALTER TABLE `lx_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `lx_order_item`
--
ALTER TABLE `lx_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT dla tabeli `lx_room`
--
ALTER TABLE `lx_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `lx_bed`
--
ALTER TABLE `lx_bed`
  ADD CONSTRAINT `FK_8D3EFF95729F519B` FOREIGN KEY (`room`) REFERENCES `lx_room` (`id`);

--
-- Ograniczenia dla tabeli `lx_order`
--
ALTER TABLE `lx_order`
  ADD CONSTRAINT `FK_F3322A768D93D649` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Ograniczenia dla tabeli `lx_order_item`
--
ALTER TABLE `lx_order_item`
  ADD CONSTRAINT `FK_E35D8CB07B6B78A9` FOREIGN KEY (`oorder`) REFERENCES `lx_order` (`id`),
  ADD CONSTRAINT `FK_E35D8CB0E647FCFF` FOREIGN KEY (`bed`) REFERENCES `lx_bed` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
