<?php

namespace Common\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Lexus\RecipeBundle\Entity\LxProduct;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LxProductsFixtures extends AbstractFixture implements ContainerAwareInterface{
    
    private $container;
    
    public function load(ObjectManager $manager){
        
        $encoderFactory = $this->container->get('security.encoder_factory');
        
        $obj = new LxProduct();
        
        $obj->setName("Marchewka");
        $obj->setCalories('5');
        $obj->setWeight("500");
        $manager->persist($obj);
        $manager->flush();
        
        $obj = new LxProduct();
        $obj->setName("Pora");
        $obj->setCalories('5');
        $obj->setWeight("500");
        $manager->persist($obj);
        $manager->flush();
        
        $obj = new LxProduct();
        $obj->setName("Kiełbasa");
        $obj->setCalories('5');
        $obj->setWeight("500");
        $manager->persist($obj);
        $manager->flush();
        
        $obj = new LxProduct();
        $obj->setName("Kurczak");
        $obj->setCalories('5');
        $obj->setWeight("500");

        $manager->persist($obj);
        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

}
