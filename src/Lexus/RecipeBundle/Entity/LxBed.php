<?php

namespace Lexus\RecipeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="lx_bed")
 * @ORM\Entity(repositoryClass="Lexus\RecipeBundle\Repository\LxBedRepository")
 * 
 */
class LxBed {

	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="bed_number", type="integer", nullable=false)
	 */
	private $bedNumber;

	/**
	 * @var Lexus\RecipeBundle\Entity\LxRoom
	 *
	 * @ORM\ManyToOne(targetEntity="Lexus\RecipeBundle\Entity\LxRoom", inversedBy="beds")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="room", referencedColumnName="id", nullable=false)
	 * })
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 */
	private $room;

	/**
	 * @var date $day
	 *
	 * @ORM\Column(name="day", type="date", nullable=false)
	 */
	private $day;
	
	/**
	 * @var boolean
	 * @ORM\Column(name="reserved", type="boolean", nullable=true)
	 *
	 */
	private $reserved = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bedNumber
     *
     * @param integer $bedNumber
     *
     * @return LxBed
     */
    public function setBedNumber($bedNumber)
    {
        $this->bedNumber = $bedNumber;

        return $this;
    }

    /**
     * Get bedNumber
     *
     * @return integer
     */
    public function getBedNumber()
    {
        return $this->bedNumber;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return LxBed
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set room
     *
     * @param \Lexus\RecipeBundle\Entity\LxRoom $room
     *
     * @return LxBed
     */
    public function setRoom(\Lexus\RecipeBundle\Entity\LxRoom $room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \Lexus\RecipeBundle\Entity\LxRoom
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set reserved
     *
     * @param boolean $reserved
     *
     * @return LxBed
     */
    public function setReserved($reserved)
    {
        $this->reserved = $reserved;

        return $this;
    }

    /**
     * Get reserved
     *
     * @return boolean
     */
    public function getReserved()
    {
        return $this->reserved;
    }
}
