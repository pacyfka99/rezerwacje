<?php

namespace Lexus\RecipeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="lx_product")
 * @ORM\Entity(repositoryClass="Lexus\RecipeBundle\Repository\LxProductRepository")
 * 
 */

class LxProduct {

	const group_bevarages = "bevarages";
	const group_meals = "meals";
	
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
	 * @Assert\NotBlank(message = "To pole jest wymagane")
     */
    private $name;

	/**
	 * @var integer
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 * @ORM\Column(name="calories", type="integer", nullable=false)
	 */
	private $calories;
	
    /**
	 * @var
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 * @ORM\Column(name="protein", type="decimal", precision=8, scale=2, nullable=false)
	 */
	private $protein;

	/**
	 * @var double
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 * @ORM\Column(name="fat", type="decimal", precision=8, scale=2, nullable=false)
	 */
	private $fat;

	/**
	 * @var double
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 * @ORM\Column(name="carbohydrates", type="decimal", precision=8, scale=2, nullable=false)
	 */
	private $carbohydrates;

	/**
	 * @var double
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 * @ORM\Column(name="sugars", type="decimal", precision=8, scale=2, nullable=false)
	 */
	private $sugars;

	/**
	 * @var double
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 * @ORM\Column(name="salt", type="decimal", precision=8, scale=2, nullable=false)
	 */
	private $salt;

	/**
	 * @var string
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 * @ORM\Column(name="groups", type="string", length=50, nullable=false)
	 */
	private $group;

public static function getGroupArray() {
        return array(
            self::group_bevarages => "Napoje",
            self::group_meals => "Posiłki",
        );
    }
	
	 /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LxRecipe
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }


    /**
     * Set calories
     *
     * @param string $calories
     *
     * @return LxProduct
     */
    public function setCalories($calories)
    {
        $this->calories = $calories;

        return $this;
    }

    /**
     * Get calories
     *
     * @return string
     */
    public function getCalories()
    {
        return $this->calories;
    }

    
    

    

    /**
     * Set protein
     *
     * @param string $protein
     *
     * @return LxProduct
     */
    public function setProtein($protein)
    {
        $this->protein = $protein;

        return $this;
    }

    /**
     * Get protein
     *
     * @return string
     */
    public function getProtein()
    {
        return $this->protein;
    }

    /**
     * Set fat
     *
     * @param string $fat
     *
     * @return LxProduct
     */
    public function setFat($fat)
    {
        $this->fat = $fat;

        return $this;
    }

    /**
     * Get fat
     *
     * @return string
     */
    public function getFat()
    {
        return $this->fat;
    }

    /**
     * Set carbohydrates
     *
     * @param string $carbohydrates
     *
     * @return LxProduct
     */
    public function setCarbohydrates($carbohydrates)
    {
        $this->carbohydrates = $carbohydrates;

        return $this;
    }

    /**
     * Get carbohydrates
     *
     * @return string
     */
    public function getCarbohydrates()
    {
        return $this->carbohydrates;
    }

    /**
     * Set sugars
     *
     * @param string $sugars
     *
     * @return LxProduct
     */
    public function setSugars($sugars)
    {
        $this->sugars = $sugars;

        return $this;
    }

    /**
     * Get sugars
     *
     * @return string
     */
    public function getSugars()
    {
        return $this->sugars;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return LxProduct
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set group
     *
     * @param string $group
     *
     * @return LxProduct
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }
}
