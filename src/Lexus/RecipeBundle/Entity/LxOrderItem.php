<?php

namespace Lexus\RecipeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Assert\Valid
 */

/**
 * @ORM\Table(name="lx_order_item")
 * @ORM\Entity(repositoryClass="Lexus\RecipeBundle\Repository\LxOrderItemRepository")
  * @Assert\Callback({"Vendor\Package\Validator", "validate"})
 */
class LxOrderItem {

	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Lexus\RecipeBundle\Entity\LxOrder
	 *
	 * @ORM\ManyToOne(targetEntity="Lexus\RecipeBundle\Entity\LxOrder" , inversedBy="orderItems" )
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="oorder", referencedColumnName="id", nullable=false)
	 * })
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 */
	private $order;

	/**
	 * @var Lexus\RecipeBundle\Entity\LxBed
	 *
	 * @ORM\ManyToOne(targetEntity="Lexus\RecipeBundle\Entity\LxBed")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="bed", referencedColumnName="id", nullable=false)
	 * })
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 */
	private $bed;

	/**
	 * @var boolean
	 * @ORM\Column(name="vegetarians", type="boolean", nullable=true)
	 *
	 */
	private $vegetarians;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="person_name", type="text", nullable=true)
	 * @Assert\NotBlank(message = "To pole jest wymagane",groups={"order", "editUser"})
	 */
	private $personName;

	/**
     * @Assert\Callback(groups={"order"})
     */
	 public function validate(\Symfony\Component\Validator\Context\ExecutionContextInterface $context){

		if ($this->getBed()->getReserved()) {
		
			$context->buildViolation('Pokój jest już zarezerwowany, przejdź do zakładki pokoje')
					->atPath('personName')
					->addViolation();
		}
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 */
	public function setId($id) {
		$this->id = $id;

		return $this;
	}


	/**
	 * Set order
	 *
	 * @param \Lexus\RecipeBundle\Entity\LxOrder $order
	 *
	 * @return LxOrderItem
	 */
	public function setOrder(\Lexus\RecipeBundle\Entity\LxOrder $order) {
		$this->order = $order;

		return $this;
	}

	/**
	 * Get order
	 *
	 * @return \Lexus\RecipeBundle\Entity\LxOrder
	 */
	public function getOrder() {
		return $this->order;
	}

	/**
	 * Set bed
	 *
	 * @param \Lexus\RecipeBundle\Entity\LxBed $bed
	 *
	 * @return LxOrderItem
	 */
	public function setBed(\Lexus\RecipeBundle\Entity\LxBed $bed) {
		$this->bed = $bed;

		return $this;
	}

	/**
	 * Get bed
	 *
	 * @return \Lexus\RecipeBundle\Entity\LxBed
	 */
	public function getBed() {
		return $this->bed;
	}

	/**
	 * Set vegetarians
	 *
	 * @param boolean $vegetarians
	 *
	 * @return LxOrderItem
	 */
	public function setVegetarians($vegetarians) {
		$this->vegetarians = $vegetarians;

		return $this;
	}

	/**
	 * Get vegetarians
	 *
	 * @return boolean
	 */
	public function getVegetarians() {
		return $this->vegetarians;
	}

	/**
	 * Set personName
	 *
	 * @param string $personName
	 *
	 * @return LxOrderItem
	 */
	public function setPersonName($personName) {
		$this->personName = $personName;

		return $this;
	}

	/**
	 * Get personName
	 *
	 * @return string
	 */
	public function getPersonName() {
		return $this->personName;
	}

}
