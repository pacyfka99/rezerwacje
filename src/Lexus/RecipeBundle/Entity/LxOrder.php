<?php

namespace Lexus\RecipeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table(name="lx_order")
 * @ORM\Entity(repositoryClass="Lexus\RecipeBundle\Repository\LxOrderRepository")
 * @Assert\Callback({"Vendor\Package\Validator", "validate"})
 */

class LxOrder {

	const status_pending = "pending";
	const status_reservation = "reservation";
	const status_paid = "paid";
	const status_paid_in_half = "paid_in_half";

	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Common\UserBundle\Entity\User
	 *
	 * @ORM\ManyToOne(targetEntity="Common\UserBundle\Entity\User")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="user", referencedColumnName="id", nullable=false)
	 * })
	 * @Assert\NotBlank(message = "To pole jest wymagane")
	 */
	private $user;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="text", nullable=true)
	 * @Assert\NotBlank(message = "To pole jest wymagane",groups={"order", "editUser"})
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="short_name", type="text", nullable=true)
	 * @Assert\NotBlank(message = "To pole jest wymagane",groups={"order", "editUser"})
	 * @Assert\Length(
	 *      max = 10,
	 *      maxMessage = "Maksymalnie {{ limit }} znaków",groups={"order", "editUser"}
	 * )
	 */
	private $shortName;
	
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime", nullable=true)
	 */
	private $createdAt;

		/**
	 * @var integer
	 *
	 * @ORM\Column(name="to_pay", type="integer", nullable=true)
	 */
	private $toPay = 0;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="paid", type="integer", nullable=true)
	 */
	private $paid = 0;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="status", type="string", length=127, nullable=false)
	 */
	private $status = self::status_pending;

	/**
	 * @ORM\OneToMany(targetEntity="Lexus\RecipeBundle\Entity\LxOrderItem", mappedBy="order", cascade={"persist", "remove"})
	 * @ORM\OrderBy({"bed" = "ASC"})
	 */
	protected $orderItems;

	public static function getStatusArray() {
		return array(
			self::status_pending => "Oczekuje",
			self::status_reservation => "Rezerwacja",
			self::status_paid => "Opłacone",
			self::status_paid_in_half => "Opłacone częściowo"
		);
	}
	
	public static function getStatusAdminArray() {
		return array(
			"Rezerwacja" => self::status_reservation,
			"Opłacone" => self::status_paid,
			"Opłacone częściowo" => self::status_paid_in_half,
		);
	}

	

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return LxOrder
	 */
	public function setName($name) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set status
	 *
	 * @param string $status
	 *
	 * @return LxOrder
	 */
	public function setStatus($status) {
		$this->status = $status;

		return $this;
	}

	/**
	 * Get status
	 *
	 * @return string
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * Set user
	 *
	 * @param \Common\UserBundle\Entity\User $user
	 *
	 * @return LxOrder
	 */
	public function setUser(\Common\UserBundle\Entity\User $user) {
		$this->user = $user;

		return $this;
	}

	/**
	 * Get user
	 *
	 * @return \Common\UserBundle\Entity\User
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->orderItems = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add orderItem
	 *
	 * @param \Lexus\RecipeBundle\Entity\LxOrderItem $orderItem
	 *
	 * @return LxOrder
	 */
	public function addOrderItem(\Lexus\RecipeBundle\Entity\LxOrderItem $orderItem) {
		$this->orderItems[] = $orderItem;

		return $this;
	}

	/**
	 * Remove orderItem
	 *
	 * @param \Lexus\RecipeBundle\Entity\LxOrderItem $orderItem
	 */
	public function removeOrderItem(\Lexus\RecipeBundle\Entity\LxOrderItem $orderItem) {
		$this->orderItems->removeElement($orderItem);
	}

	/**
	 * Get orderItems
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getOrderItems() {
		return $this->orderItems;
	}

	/**
	 * Set shortName
	 *
	 * @param string $shortName
	 *
	 * @return LxOrder
	 */
	public function setShortName($shortName) {
		$this->shortName = $shortName;

		return $this;
	}

	/**
	 * Get shortName
	 *
	 * @return string
	 */
	public function getShortName() {
		return $this->shortName;
	}

	/**
	 * Set toPay
	 *
	 * @param integer $toPay
	 *
	 * @return LxOrder
	 */
	public function setToPay($toPay) {
		$this->toPay = $toPay;

		return $this;
	}

	/**
	 * Get toPay
	 *
	 * @return integer
	 */
	public function getToPay() {
		return $this->toPay;
	}

	/**
	 * Set paid
	 *
	 * @param integer $paid
	 *
	 * @return LxOrder
	 */
	public function setPaid($paid) {
		$this->paid = $paid;

		return $this;
	}

	/**
	 * Get paid
	 *
	 * @return integer
	 */
	public function getPaid() {
		return $this->paid;
	}


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return LxOrder
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
