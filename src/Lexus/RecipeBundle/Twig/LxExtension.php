<?php

namespace Lexus\RecipeBundle\Twig;

use Twig_Extension;
use Twig_SimpleFilter;

class LxExtension extends Twig_Extension {

	public function getFilters() {
		return array(
			new Twig_SimpleFilter('priceF', array($this, 'priceFilter')),
			new Twig_SimpleFilter('checkDate', array($this, 'checkDate')),
		);
	}

	public function priceFilter($number) {
		return sprintf("%06d", $number);
	}

	public function checkDate($date) {
		if ($date) {
			$date2 = clone $date;
			$date2 = $date2->modify('+2 day');
			$newDate = new \DateTime;
			if($date2->format('Y-m-d') < $newDate->format('Y-m-d')){
				return true;
			}
		}
		return false;
	}

}
?>