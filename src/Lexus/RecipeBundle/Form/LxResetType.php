<?php

namespace Lexus\RecipeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Common\UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class LxResetType extends AbstractType {

	public function getName() {
		return 'reset';
	}

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
				->add('currentPassword', PasswordType::class, array(
					'label' => 'Aktualne hasło',
					'mapped' => false,
					'required' => FALSE,
					'constraints' => array(
						new UserPassword(array(
							'message' => 'Podano błędne aktualne hasło użytkownika'
								))
					)
		));
		$builder->add('save', SubmitType::class, array('label' => 'Reset'));
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => User::class,
//			'validation_groups' => array('Default', 'ChangePassword')
		));
	}

}
