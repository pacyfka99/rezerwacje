<?php

namespace Lexus\RecipeBundle\Form;

use Lexus\RecipeBundle\Entity\LxOrderItem;
use Lexus\RecipeBundle\Entity\LxProduct;
use Lexus\RecipeBundle\Entity\LxRecipe;
use Lexus\RecipeBundle\Repository\LxProductRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

class LxOrderItemType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
	

		$disabled = false;
		if ($options['form_type'] == 'delete')
			$disabled = true;

		$builder->add('id', HiddenType::class, array(
			'label' => "",
			'required' => false,
			'disabled' => $disabled,
		));
	
		$builder->add('vegetarians', CheckboxType::class, array(
			'label' => "Posiłek wegetariański",
			'required' => false,
			'disabled' => $disabled,
		));
		
		$builder->add('personName', TextType::class, array(
			'label' => "Imię i Nazwisko",
			'required' => false,
			'disabled' => $disabled,
			
		));
		
		$builder->add('bed', LxBedCollectionType::class, array(
			'label' => "Pokój",
			'constraints' => array(new Valid()),
		));






//		$builder->add('save', SubmitType::class, array('label' => 'Zapisz'));

//		$builder->add('product', LxProductType::class);
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => LxOrderItem::class,
//			'validation_groups' => array('order'),
			'cascade_validation' => true,
			'form_type' => null,
		));
	}

	public function getName() {
		return 'orderItemsForm';
	}

}
