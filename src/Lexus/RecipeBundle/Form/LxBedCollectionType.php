<?php

namespace Lexus\RecipeBundle\Form;

use Lexus\RecipeBundle\Entity\LxBed;
use Lexus\RecipeBundle\Repository\LxProductRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

class LxBedCollectionType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
	
		$disabled = false;

	
		
		$builder->add('bedNumber', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class, array(
			'label' => false,
			'required' => false,
			'disabled' => true,
			'attr' => ['readonly' => true], 
	
		));
		$builder->add('day', \Symfony\Component\Form\Extension\Core\Type\DateType::class, array(
			'label' => false,
			'required' => false,
			'disabled' => true,
			'attr' => ['readonly' => true],  
			
		));
		
		$builder->add('room', LxRoomCollectionType::class, array(
			'label' => false,
		));
	






//		$builder->add('save', SubmitType::class, array('label' => 'Zapisz'));

//		$builder->add('product', LxProductType::class);
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => LxBed::class,
		));
	}

	public function getName() {
		return 'bedCollectionForm';
	}

}
