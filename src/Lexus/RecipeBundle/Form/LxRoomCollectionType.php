<?php

namespace Lexus\RecipeBundle\Form;

use Lexus\RecipeBundle\Entity\LxBed;
use Lexus\RecipeBundle\Entity\LxRoom;
use Lexus\RecipeBundle\Repository\LxProductRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

class LxRoomCollectionType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
	
		$disabled = false;

		$builder->add('name', TextType::class, array(
			'label' => false,
			'required' => false,
			'disabled' => $disabled,
			'attr' => ['readonly' => true],  
		));
	
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => LxRoom::class,
		));
	}

	public function getName() {
		return 'roomCollectionForm';
	}

}
