<?php

namespace Lexus\RecipeBundle\Form;

use Lexus\RecipeBundle\Entity\LxProduct;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LxProductType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
$disabled = false;
		$builder->add('name', TextType::class, array(
			'label' => "Nazwa",
			'required' => true,
			'disabled' => $disabled,
		));
		$builder->add('calories', IntegerType::class, array(
			'label' => "Kalorie",
			'required' => false,
			'disabled' => $disabled,
		));
		$builder->add('protein', NumberType::class, array(
			'scale' => 2,
			'label' => "Białka",
			'required' => false,
			'disabled' => $disabled,
		));
		$builder->add('fat', NumberType::class, array(
			'scale' => 2,
			'label' => "Tłuszcz",
			'required' => false,
			'disabled' => $disabled,
		));
		$builder->add('carbohydrates', NumberType::class, array(
			'scale' => 2,
			'label' => "Węglowodany",
			'required' => false,
			'disabled' => $disabled,
		));
		$builder->add('sugars', NumberType::class, array(
			'scale' => 2,
			'label' => "Cukry",
			'required' => false,
			'disabled' => $disabled,
		));
		$builder->add('salt', NumberType::class, array(
			'scale' => 2,
			'label' => "Sól",
			'required' => false,
			'disabled' => $disabled,
		));		
       $builder->add('group', ChoiceType::class, array(
            'choices' => array_flip(LxProduct::getGroupArray()), 'placeholder' => 'brak'
        ));
	
        $builder->add('save', SubmitType::class, array('label' => 'Zapisz'));
    }

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => LxProduct::class,
		));
	}
//    public function setDefaultOptions(OptionsResolverInterface $resolver) {
//        $resolver->setDefaults(array(
//            'data_class' => 'LexusProductBundle\Entity\LxProduct',
//        ));
//    }

    public function getName() {
        return 'productForm';
    }

}
