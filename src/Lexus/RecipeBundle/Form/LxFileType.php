<?php

namespace Lexus\RecipeBundle\Form;

use Lexus\RecipeBundle\Entity\LxProduct;
use Lexus\RecipeBundle\Repository\LxProductRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LxFileType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$disabled = false;
		
        $builder->add('name', TextType::class, array(
			'label' => "Nazwa pliku",
			'required' => false,
			'disabled' => $disabled,
		));
        $builder->add('file', FileType::class, array(
			'label' => "Plik",
			'required' => false,
			'disabled' => $disabled,
		));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LexusRecipeBundle\Entity\LxDocument',
        ));
    }

    public function getName()
    {
        return 'fileForm';
    }

}
