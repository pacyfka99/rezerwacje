<?php

namespace Lexus\RecipeBundle\Form;

use Lexus\RecipeBundle\Entity\LxProduct;
use Lexus\RecipeBundle\Entity\LxRecipe;
use Lexus\RecipeBundle\Entity\LxRecipeProduct;
use Lexus\RecipeBundle\Repository\LxProductRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApRecipeProductCollectionType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {

		$builder->add('product', EntityType::class, array(
			'class' => 'LexusRecipeBundle:LxProduct',
			'query_builder' => function (LxProductRepository $er) {
				return $er->getObj();
			},
			'choice_label' => 'name',
		));
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => LxRecipeProduct::class,
		));
	}

	public function getName() {
		return 'recipeProductForm';
	}

}
