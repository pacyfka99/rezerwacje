<?php

namespace Lexus\RecipeBundle\Form;

use Lexus\RecipeBundle\Entity\LxOrder;
use Lexus\RecipeBundle\Entity\LxProduct;
use Lexus\RecipeBundle\Entity\LxRecipe;
use Lexus\RecipeBundle\Repository\LxProductRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

class LxOrderType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {

		$disabled = false;
		if ($options['form_type'] == 'delete')
			$disabled = true;

		$builder->add('name', TextType::class, array(
			'label' => "Nazwa klubu",
			'required' => false,
			'disabled' => $disabled,
		));
		$builder->add('shortName', TextType::class, array(
			'label' => "Skrócona nazwa klubu",
			'required' => false,
			'disabled' => $disabled,
		));

		$builder->add('orderItems', CollectionType::class, array(
			'entry_type' => LxOrderItemType::class,
			'label' => false,
			'allow_add' => true,
			'allow_delete' => true,
			'prototype' => true,
			'constraints' => array(new Valid()),
			'entry_options' => array('form_type' => $options['form_type']),
//			// these options are passed to each "email" type
//			'entry_options' => array(
//				'attr' => array('class' => 'email-box')
//			),
		));
		
		if ($options['form_type'] == 'edit'){
			$builder->add('status', ChoiceType::class, array(
			'label' => "Status",
			'choices' => LxOrder::getStatusAdminArray(),
			'disabled' => $disabled
			));
			$builder->add('paid', NumberType::class, array(
				'label' => "Zapłacono",
				'required' => false,
				'disabled' => $disabled,
			));
			
			
		}

		$save = 'Przejdź do płatności';
		$cancel = 'Zapisz i wróć do listy pokoi';
		if ($options['form_type'] == 'delete') {
			$save = 'Usuń';
			$cancel = 'Anuluj';
		}
		if ($options['form_type'] == 'edit' || $options['form_type'] == 'editUser') {
			$save = 'Zapisz';
			$cancel = 'Anuluj';
		}

		$builder->add('cancel', SubmitType::class, array('label' => $cancel));
		$builder->add('save', SubmitType::class, array('label' => $save));

//		$builder->add('product', LxProductType::class);
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => LxOrder::class,
			'validation_groups' => array('order'),
			'cascade_validation' => true,
			'form_type' => null,
		));
	}

	public function getName() {
		return 'orderForm';
	}

}
