<?php

namespace Lexus\RecipeBundle\Controller;

use Common\Core\LxController;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Lexus\RecipeBundle\Entity\LxBed;
use Lexus\RecipeBundle\Entity\LxOrder;
use Lexus\RecipeBundle\Entity\LxOrderItem;
use Lexus\RecipeBundle\Form\LxOrderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class LxHomeController extends LxController {

	/**
	 * @Route("/", name="home")
	 */
	public function indexAction(Request $request) {
		return $this->redirectToRoute('rooms');
		return $this->render('default/index.html.twig', [
//                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
		]);
	}

	/**
	 * @Route("/pokoje", name="rooms")
	 */
	public function roomsAction(Request $request) {

		$rooms = $this->getLxRoomRepository()->findAll();

		$activeOrder = $this->getLxOrderRepository()->findOneBy(array('status' => LxOrder::status_pending, 'user' => $this->getUser()->getId()));
		$activeItem = $this->getLxOrderItemRepository()->getActiveItem();
		$bedReservedFalse = $this->getLxBedRepository()->findBy(array('reserved' => null));
		$blockedBed = array();
		$blockedRoom = array();
		foreach ($bedReservedFalse as $item) {
			$item instanceof LxBed;
			$blockedRoom[$item->getRoom()->getId()] = $item->getRoom()->getId();
		}

		foreach ($activeItem as $item) {
			$blockedBed[$item->getBed()->getId()] = $item->getOrder()->getShortName();
		}

		$mybeds = array();
		if ($activeOrder) {
			foreach ($activeOrder->getOrderItems() as $item) {
				$mybeds[$item->getBed()->getId()] = $item->getBed()->getId();
			}
		}

		if ($request->isMethod('POST')) {
			$em = $this->getDoctrine()->getManager();

			$order = $activeOrder;

			if (!$activeOrder)
				$order = new LxOrder();
			$requestTab = array();
			foreach ($request->request as $k => $v) {
				$requestTab[$k] = $k;
			}

			if (count($requestTab) == 0) {
				$this->get('session')->getFlashBag()->add('danger', 'Wybierz nocleg');
				return $this->redirectToRoute('rooms');
			}

			foreach ($order->getOrderItems() as $item) {
				if (!isset($requestTab[$item->getBed()->getId()])) {
					$em->remove($item);
					$order->getOrderItems()->removeElement($item);
				} else {

					unset($requestTab[$item->getBed()->getId()]);
				}
			}
			$order->setUser($this->getUser());
			$em->persist($order);
			$em->flush();

			foreach ($requestTab as $k => $v) {
				$orderItem = new LxOrderItem();
				$bed = $this->getLxBedRepository()->find($k);
				$orderItem->setBed($bed);
				$orderItem->setOrder($order);
				$em->persist($orderItem);
			}
			$em->flush();




			return $this->redirectToRoute('order');
		}

		return $this->render('LexusRecipeBundle:Home:rooms.html.twig', [
					'rooms' => $rooms,
					'mybeds' => $mybeds,
					'blockedBed' => $blockedBed,
					'blockedRoom' => $blockedRoom
//                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
		]);
	}

	/**
	 * @Route("/zamowienie", name="order")
	 */
	public function orderAction(Request $request) {
		$obj = $this->getLxOrderRepository()->findOneBy(array('status' => LxOrder::status_pending, 'user' => $this->getUser()->getId()));

		if (!$obj) {
			$request->getSession()
					->getFlashBag()
					->add('danger', 'Brak aktywnego zamówienia')
			;
			return $this->redirectToRoute('rooms');
		}

		$obj instanceof LxOrder;
		if (count($obj->getOrderItems()) == 0) {
			$this->get('session')->getFlashBag()->add('danger', 'Wybierz nocleg');
			return $this->redirectToRoute('rooms');
		}
		$first = 0;
		$secoud = 0;
		foreach ($obj->getOrderItems() as $item) {
			$item instanceof LxOrderItem;

			if ($item->getBed()->getDay()->format('Y-m-d') == '2018-11-17') {
				$first = $first + 1;
			} else {
				$secoud = $secoud + 1;
			}
		}
	
		if ($first == $secoud) {
			$obj->setToPay($first * 120);
		} elseif ($first > $secoud) {
			$obj->setToPay($secoud * 120);
			$resultPrice = $first - $secoud;
			$obj->setToPay($obj->getToPay() + $resultPrice * 40);
		} else {
			$obj->setToPay($first * 120);
			$resultPrice = $secoud - $first;
			$obj->setToPay($obj->getToPay() + $resultPrice * 85);
		}

		$options['form_type'] = 'add';
		$form = $this->createForm(LxOrderType::class, $obj, $options);
		$form->handleRequest($request);

		$em = $this->getDoctrine()->getManager();
		$em->persist($obj);
		$em->flush();
//		print_r($request->get('cancel'));
//		die;
		if ($form->get('cancel')->isClicked())
			return $this->redirect($this->generateUrl('rooms'));

		if ($form->isSubmitted() && $form->isValid()) {

			$em = $this->getDoctrine()->getManager();
			$obj->setStatus(LxOrder::status_reservation);
			foreach ($obj->getOrderItems() as $item) {
				$item instanceof LxOrderItem;
				$item->getBed()->setReserved(true);
				$em->persist($item->getBed());
			}
			$obj->setCreatedAt(new DateTime);
			$em->persist($obj);
			$em->flush();
			$this->get('session')->getFlashBag()->add('success', 'Zamówienie zostało zrealizowane');
			return $this->redirectToRoute('orders');
		}
		return $this->render('LexusRecipeBundle:Home:order.html.twig', [
					'obj' => $obj,
					'form' => $form->createView(),
					'form_type' => $options['form_type']
//					'table' => $table->run(),
//                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
		]);
	}

	/**
	 * @Route("/zamowienia", name="orders")
	 */
	public function ordersAction(Request $request) {
		$orders = $this->getLxOrderRepository()->getOrderReservedByUser($this->getUser()->getId());
		$cratedAt = null;
		foreach ($orders as $item) {
			if ($item->getCreatedAt()) {
				if ($cratedAt == null) {
					$cratedAt = $item->getCreatedAt();
					continue;
				}
				if ($item->getCreatedAt() >= $cratedAt)
					$cratedAt = $item->getCreatedAt();
			}
		}

		if ($cratedAt) {
			$cratedAt = $cratedAt->modify('+2 day');
			$cratedAt = $cratedAt->format('Y-m-d');
		}
		$id = $this->getUser()->getId();


		return $this->render('LexusRecipeBundle:Home:orders.html.twig', [
					'orders' => $orders,
					'orderid' => sprintf("%06d", $id),
					'cratedAt' => $cratedAt
//					'form' => $form->createView(),
//					'table' => $table->run(),
//                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
		]);
	}
	
	
	/**
	 * @Route("/admin", name="admin")
	 * @Security("is_granted('ROLE_ADMIN')")
	 */
	public function adminAction(Request $request) {

		$orders = $this->getLxOrderRepository()->getActiveOrder();
		$vegetarinas = $this->getLxOrderItemRepository()->findVegetarians();
		$allItems = $this->getLxOrderItemRepository()->getActiveItem();

		$allMeals = 0;
		if ($allItems)
			$allMeals = count($allItems);

		$vegetariansCount = 0;
		if ($vegetarinas)
			$vegetariansCount = count($vegetarinas);

		return $this->render('LexusRecipeBundle:Home:admin_orders.html.twig', [
					'orders' => $orders,
					'vegetariansCount' => $vegetariansCount,
					'allMeals' => $allMeals,
//                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
		]);
	}

	/**
	 * @Route("/admin2", name="admin2")
	 * @Security("is_granted('ROLE_ADMIN')")
	 */
	public function admin2Action(Request $request) {
		$orders = $this->getLxOrderRepository()->getActiveOrder();
		$vegetarinas = $this->getLxOrderItemRepository()->findVegetarians();
		$vegetariansCount = 0;
		if ($vegetarinas)
			$vegetariansCount = count($vegetarinas);

		$allItems = $this->getLxOrderItemRepository()->getActiveItem();
		$allMeals = 0;
		if ($allItems)
			$allMeals = count($allItems);

		return $this->render('LexusRecipeBundle:Home:admin_orders2.html.twig', [
					'orders' => $orders,
					'vegetariansCount' => $vegetariansCount,
					'allMeals' => $allMeals,
//                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
		]);
	}

	/**
	 * @Route("/admin3", name="admin3")
	 * @Security("is_granted('ROLE_ADMIN')")
	 */
	public function admin3Action(Request $request) {
		$rooms = $this->getLxRoomRepository()->findAll();

		$activeOrder = $this->getLxOrderRepository()->findOneBy(array('status' => LxOrder::status_pending, 'user' => $this->getUser()->getId()));
		$activeItem = $this->getLxOrderItemRepository()->getActiveItem();
		$bedReservedFalse = $this->getLxBedRepository()->findBy(array('reserved' => null));
		$blockedBed = array();
		$blockedRoom = array();
		foreach ($bedReservedFalse as $item) {
			$item instanceof LxBed;
			$blockedRoom[$item->getRoom()->getId()] = $item->getRoom()->getId();
		}

		foreach ($activeItem as $item) {
			$vege = $item->getVegetarians() == false ? 'Nie' : 'Tak';
			$blockedBed[$item->getBed()->getId()] = $item->getOrder()->getShortName() . ' | ' . $item->getPersonName() . ' | ' . $vege;
		}

		$mybeds = array();
//		if ($activeOrder) {
//			foreach ($activeOrder->getOrderItems() as $item) {
//				$mybeds[$item->getBed()->getId()] = $item->getBed()->getId();
//			}
//		}


		return $this->render('LexusRecipeBundle:Home:admin_orders3.html.twig', [
					'rooms' => $rooms,
					'mybeds' => $mybeds,
					'blockedBed' => $blockedBed,
					'blockedRoom' => $blockedRoom
//                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
		]);
	}

	/**
	 * @Route("/admin-delete/{id}", name="admin_delete")
	 * @Security("is_granted('ROLE_ADMIN')")
	 */
	public function adminDeleteAction(Request $request, LxOrder $obj) {

		$options['form_type'] = 'delete';
		$form = $this->createForm(LxOrderType::class, $obj, $options);
		$form->handleRequest($request);


		if ($form->get('cancel')->isClicked())
			return $this->redirect($this->generateUrl('admin'));

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$em = $this->getDoctrine()->getManager();
				$em->getConnection()->beginTransaction();
				foreach ($obj->getOrderItems() as $item) {
					$item instanceof LxOrderItem;
					$item->getBed()->setReserved(NULL);
					$em->persist($item->getBed());
				}
				$em->remove($obj);
				$em->flush();
				$em->getConnection()->commit();
				$this->get('session')->getFlashBag()->add('success', 'Zamówienie zostało usunięte');
				return $this->redirectToRoute('admin');
			} catch (Exception $e) {
				$em->getConnection()->rollBack();
				throw $this->createNotFoundException('Nie udało się usunąć zamówienia');
			}
		}
		return $this->render('LexusRecipeBundle:Home:order.html.twig', [
					'obj' => $obj,
					'form' => $form->createView(),
					'form_type' => $options['form_type']
//					'table' => $table->run(),
//                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
		]);
	}

	/**
	 * @Route("/admin-edit/{id}", name="admin_edit")
	 * @Security("is_granted('ROLE_ADMIN')")
	 */
	public function adminEditAction(Request $request, LxOrder $obj) {

		$options['form_type'] = 'edit';
		$options['validation_groups'] = array('admin');
		$form = $this->createForm(LxOrderType::class, $obj, $options);

		$orginalItems = new ArrayCollection();
		foreach ($obj->getOrderItems() as $item) {
			$orginalItems->add($item);
		}
		$form->handleRequest($request);


		if ($form->get('cancel')->isClicked())
			return $this->redirect($this->generateUrl('admin'));

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$em = $this->getDoctrine()->getManager();
				$em->getConnection()->beginTransaction();
//				

				foreach ($orginalItems as $item) {
					if (false === $obj->getOrderItems()->contains($item)) {
						$item instanceof LxOrderItem;
						$item->getBed()->setReserved(NULL);
						$em->persist($item->getBed());
						$obj->removeOrderItem($item);
						$em->remove($item);
					}
				}

				$first = 0;
				$secoud = 0;
				foreach ($obj->getOrderItems() as $item) {
					$item instanceof LxOrderItem;
					if ($item->getBed()->getDay()->format('Y-m-d') == '2017-11-17') {
						$first = $first + 1;
					} else {
						$secoud = $secoud + 1;
					}
				}
				if ($first == $secoud) {
					$obj->setToPay($first * 120);
				} elseif ($first > $secoud) {
					$obj->setToPay($secoud * 120);
					$resultPrice = $first - $secoud;
					$obj->setToPay($obj->getToPay() + $resultPrice * 40);
				} else {
					$obj->setToPay($first * 120);
					$resultPrice = $secoud - $first;
					$obj->setToPay($obj->getToPay() + $resultPrice * 85);
				}

				$em->persist($obj);
				$em->flush();
				$em->getConnection()->commit();
				$this->get('session')->getFlashBag()->add('success', 'Zamówienie zostało zapisane');
				return $this->redirectToRoute('admin');
			} catch (Exception $e) {
				$em->getConnection()->rollBack();
				throw $this->createNotFoundException('Nie udało się usunąć zamówienia');
			}
		}
		return $this->render('LexusRecipeBundle:Home:order.html.twig', [
					'obj' => $obj,
					'form' => $form->createView(),
					'form_type' => $options['form_type'],
//					'table' => $table->run(),
//                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
		]);
	}
	/**
	 * @Route("/user-edit-order/{id}", name="user_edit_order")
	 * @Security("is_granted('ROLE_USER')")
	 */
	public function userEditOrderAction(Request $request, LxOrder $obj) {

		if($obj->getUser()->getId() != $obj->getUser()->getId())
			throw $this->createNotFoundException('Brak uprawnień');
		if($obj->getStatus() != LxOrder::status_reservation)
			throw $this->createNotFoundException('Brak uprawnień');
		
		$options['form_type'] = 'editUser';
		$options['validation_groups'] = array('editUser');
		$form = $this->createForm(LxOrderType::class, $obj, $options);

		$orginalItems = new ArrayCollection();
		foreach ($obj->getOrderItems() as $item) {
			$orginalItems->add($item);
		}
		$form->handleRequest($request);


		if ($form->get('cancel')->isClicked())
			return $this->redirect($this->generateUrl('orders'));

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$em = $this->getDoctrine()->getManager();
				$em->getConnection()->beginTransaction();
//				

				foreach ($orginalItems as $item) {
					if (false === $obj->getOrderItems()->contains($item)) {
						$item instanceof LxOrderItem;
						$item->getBed()->setReserved(NULL);
						$em->persist($item->getBed());
						$obj->removeOrderItem($item);
						$em->remove($item);
					}
				}

				$first = 0;
				$secoud = 0;
				foreach ($obj->getOrderItems() as $item) {
					$item instanceof LxOrderItem;
					if ($item->getBed()->getDay()->format('Y-m-d') == '2017-11-17') {
						$first = $first + 1;
					} else {
						$secoud = $secoud + 1;
					}
				}
				if ($first == $secoud) {
					$obj->setToPay($first * 120);
				} elseif ($first > $secoud) {
					$obj->setToPay($secoud * 120);
					$resultPrice = $first - $secoud;
					$obj->setToPay($obj->getToPay() + $resultPrice * 40);
				} else {
					$obj->setToPay($first * 120);
					$resultPrice = $secoud - $first;
					$obj->setToPay($obj->getToPay() + $resultPrice * 85);
				}

				$em->persist($obj);
				$em->flush();
				$em->getConnection()->commit();
				$this->get('session')->getFlashBag()->add('success', 'Zamówienie zostało zapisane');
				return $this->redirectToRoute('orders');
			} catch (Exception $e) {
				$em->getConnection()->rollBack();
				throw $this->createNotFoundException('Nie udało się zmienić zamówienia');
			}
		}
		return $this->render('LexusRecipeBundle:Home:order.html.twig', [
					'obj' => $obj,
					'form' => $form->createView(),
					'form_type' => $options['form_type'],
//					'table' => $table->run(),
//                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
		]);
	}
	
	
	
	
	/**
	 * @Route("/admin-reset", name="admin_reset")
	 * @Security("is_granted('ROLE_SUPER_ADMIN')")
	 */
	public function adminResetAction(Request $request) {

		$form = $this->createForm(\Lexus\RecipeBundle\Form\LxResetType::class, $this->getUser());
		$form->handleRequest($request);
//		if ($form->get('cancel')->isClicked())
//			return $this->redirect($this->generateUrl('orders'));

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$this->getLxOrderItemRepository()->deleteAll();
				$this->getLxOrderRepository()->deleteAll();
				$this->getLxBedRepository()->resetAll();
				$this->getUsersRepository()->resetAll();
				
				
				$this->get('session')->getFlashBag()->add('success', 'Baza danych została zresetowana');
				return $this->redirectToRoute('admin_reset');
			} catch (Exception $e) {
				$em->getConnection()->rollBack();
				throw $this->createNotFoundException('Nie udało się zresetować ustawień');
			}
		}
		return $this->render('LexusRecipeBundle:Home:admin_reset.html.twig', [
					'form' => $form->createView(),
		]);
	}

}
