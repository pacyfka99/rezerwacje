<?php

namespace Lexus\RecipeBundle\Controller;

use Common\Core\LxController;
use Doctrine\Common\Collections\ArrayCollection;
use Lexus\RecipeBundle\Entity\LxDocument;
use Lexus\RecipeBundle\Entity\LxRecipe;
use Lexus\RecipeBundle\Entity\LxRecipeProduct;
use Lexus\RecipeBundle\Form\LxRecipeType;
use Lexus\RecipeBundle\Table\LxRecipeTable;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class LxRecipeController extends LxController {

	/**
	 * @Route("/recipe-index", name="recipe")
	 */
	public function indexAction(Request $request) {


		$table = new LxRecipeTable($this->getData());
		$addLink = $this->generateUrl('recipe_add');

		return $this->render('LexusRecipeBundle:Default:index.html.twig', [
					'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
					'table' => $table->run(),
		]);
	}

	/**
	 * @Route("/recipe-add", name="lxrecipe_add")
	 */
	public function addAction(Request $request) {
		$obj = new LxRecipe();


		$form = $this->createForm(LxRecipeType::class, $obj);
		$form->handleRequest($request);


		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();

			$file = new LxDocument();
			$file->setRecipe($obj);
			$file->setName($obj->getFiles()['name']);
			$file->setFile($obj->getFiles()['file']);
			foreach ($obj->getProducts() as $product) {
				$recipeProduct = new LxRecipeProduct();
				$recipeProduct->setProduct($product->getProduct());
				$recipeProduct->setRecipe($obj);
				$em->persist($recipeProduct);
			}

			$em->persist($file);
			$em->flush();

			$em->persist($obj);
			$em->flush();

			return $this->redirectToRoute('recipe');
		}

		return $this->render('LexusRecipeBundle:recipe:form.html.twig', array(
					'form' => $form->createView(),
		));
	}

	/**
	 * @Route("/recipe-edit/{id}", name="lxrecipe_edit")
	 */
	public function editAction(Request $request, LxRecipe $obj) {


		$arrayCollection = array();
		foreach ($obj->getRecipeProducts() as $product) {
			$arrayCollection[$product->getId()] = $product;
			if (!$request->isMethod('POST'))
				$obj->addProduct($product);
		}

		$form = $this->createForm(LxRecipeType::class, $obj);
		$form->handleRequest($request);


		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();

			foreach ($obj->getProducts() as $item) {
				if ($item->getProduct()->getId() && isset($arrayCollection[$item->getProduct()->getId()])) {
					unset($arrayCollection[$item->getId()]);
				} else {
					$recipeProduct = new LxRecipeProduct();
					$recipeProduct->setProduct($item->getProduct());
					$recipeProduct->setRecipe($obj);
					$em->persist($recipeProduct);
				}
			}

			foreach ($arrayCollection as $item)
				$em->remove($item);


			$em->persist($obj);
			$em->flush();

			return $this->redirectToRoute('recipe');
		}

		return $this->render('LexusRecipeBundle:Recipe:form.html.twig', array(
					'form' => $form->createView(),
		));
	}

	/**
	 * @Route("/recipe-delete", name="lxrecipe_delete")
	 */
	public function deleteAction(Request $request) {
		die;
	}

}
