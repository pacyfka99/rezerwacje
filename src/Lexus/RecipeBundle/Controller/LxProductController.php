<?php

namespace Lexus\RecipeBundle\Controller;

use Common\Core\LxController;
use Lexus\RecipeBundle\Entity\LxProduct;
use Lexus\RecipeBundle\Form\LxProductType;
use Lexus\RecipeBundle\Table\LxProductTable;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class LxProductController extends LxController {

	/**
	 * @Route("/product-index", name="product")
	 */
	public function indexAction(Request $request) {


		$table = new LxProductTable($this->getData());

		return $this->render('LexusRecipeBundle:Default:index.html.twig', [
					'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
					'table' => $table->run(),
		]);
	}

	/**
	 * @Route("/product-add", name="lxproduct_add")
	 */
	public function addAction(Request $request) {


		$obj = new LxProduct();

		$form = $this->createForm(LxProductType::class, $obj);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($obj);
			$em->flush();

			return $this->redirectToRoute('product');
		}

		return $this->render('LexusRecipeBundle:product:form.html.twig', array(
					'form' => $form->createView(),
		));
	}

	/**
	 * @Route("/product-edit/{id}", name="lxproduct_edit")
	 */
	public function editAction(Request $request, LxProduct $obj) {

		$form = $this->createForm(LxProductType::class, $obj);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($obj);
			$em->flush();

			return $this->redirectToRoute('product');
		}

		return $this->render('LexusRecipeBundle:product:form.html.twig', array(
					'form' => $form->createView(),
		));
	}

	/**
	 * @Route("/product-delete/{id}", name="lxproduct_delete")
	 */
	public function deleteAction(Request $request) {
		die;
		return ;
	}

}
