<?php

namespace Lexus\RecipeBundle\Controller;

use Common\Core\LxController;
use Lexus\RecipeBundle\Entity\LxDocument;
use Lexus\RecipeBundle\Form\LxFileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class LxDocumentController extends LxController {

    /**
     * @Route("/document", name="document")
     */
    public function indexAction(Request $request) {

        return $this->render('default/index.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
        ]);
    }
    
    /**
     * @Route("/document-add", name="document_add")
     */
    public function addAction(Request $request) {
     

        $obj = new LxDocument();
        $form = $this->createForm(LxFileType::class, $obj);
//        $form = $this->createFormBuilder($obj)
//        ->add('name')
//        ->add('file')
//        ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//        print_r($obj);
//        die;
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
//            $task = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
    
             $em = $this->getDoctrine()->getManager();
             $em->persist($obj);
             $em->flush();

            return $this->redirectToRoute('document');
        }

        return $this->render('LexusRecipeBundle:document:form.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}
