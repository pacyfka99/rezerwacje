<?php

namespace Lexus\RecipeBundle\Controller;

use Common\Core\LxController;
use Lexus\RecipeBundle\Entity\LxDocument;
use Lexus\RecipeBundle\Entity\LxRecipe;
use Lexus\RecipeBundle\Form\LxRecipeType;
use Lexus\RecipeBundle\Table\LxRecipeTable;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LxMealsController extends LxController {

	/**
	 * @Route("/meals-index", name="meals")
	 */
	public function indexAction(Request $request) {

		$meals = array(
			0 => array(LxRecipe::brekfast => array()),
			1 => array(LxRecipe::dinner => array()),
			2 => array(LxRecipe::supper => array()),
			3 => array(LxRecipe::snacks => array()),
		);

		$mealsCurrent = $this->getLxRecipeRepository()->getList()->getQuery()->getResult();
		$mealDb = array(
			LxRecipe::brekfast => LxRecipe::brekfast,
			LxRecipe::dinner => LxRecipe::dinner,
			LxRecipe::supper => LxRecipe::supper,
			LxRecipe::snacks => LxRecipe::snacks,
		);

		foreach ($mealDb as $k => $v) {
			$mealsCurrent = array();
			$mealsCurrent = $this->getLxRecipeRepository()->getList($k)->getQuery()->getResult();
			$mealsData = array();
			foreach ($mealsCurrent as $value) {
				$mealsData[$value['p_id']]['name'] = $value['p_name'];

				$dir = 'uploads/documents/' . $value['d_path'];
				$base64 = base64_encode(file_get_contents(__DIR__ . '/../../../web/' . $dir));
				$mealsData[$value['p_id']]['img'] = $base64;
			}
			$mealDb[$k] = $mealsData;
		}

		$diet = $this->getLxRecipeDietRepository()->getDietByUser($this->getUser());
	
		foreach ($diet as $k => $item) {
			foreach ($item['dietDays'] as $k2 =>$item2) {
				foreach ($item2['dietDayRecipes'] as $k3 => $item3) {
					$dir = 'uploads/documents/' . $item3['recipe']['image']['path'];
					$base64 = base64_encode(file_get_contents(__DIR__ . '/../../../web/' . $dir));
					$diet[$k]['dietDays'][$k2]['dietDayRecipes'][$k3]['recipe']['image']['img'] = $base64;
				}
			}
		}

		return $this->render('LexusRecipeBundle:Meals:index.html.twig', [
					'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
					LxRecipe::brekfast => json_encode($mealDb[LxRecipe::brekfast]),
					LxRecipe::snacks => json_encode($mealDb[LxRecipe::snacks]),
					LxRecipe::dinner => json_encode($mealDb[LxRecipe::dinner]),
					LxRecipe::supper => json_encode($mealDb[LxRecipe::supper]),
					'kindOfMeals' => json_encode($meals),
					'diet' => json_encode($diet),
		]);
	}

	/**
	 * @Route("/meals-add", name="recipe_add")
	 */
	public function addAction(Request $request) {


		$obj = new LxRecipe();

		$form = $this->createForm(LxRecipeType::class, $obj);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
		

			$file = new LxDocument();
			$file->setRecipe($obj);
			$file->setName($obj->getFiles()['name']);
			$file->setFile($obj->getFiles()['file']);
			$obj->setImage($file);
	
			$em->persist($file);
			$em->flush();
			$em->persist($obj);
			$em->flush();

			return $this->redirectToRoute('recipe');
		}

		return $this->render('LexusRecipeBundle:recipe:form.html.twig', array(
					'form' => $form->createView(),
		));
	}

	/**
	 * @Route("/get-meals", name="meals_json")
	 */
	public function mealsJsonAction(Request $request) {
		try {
			$data = array("test", "test2");

			$mealsCurrent = $this->getLxRecipeRepository()->getList()->getQuery()->getResult();
			$kind = $request->request->get('kind');

			if ($kind == "brekfast") {
				$mealDb = array(
					'brekfast' => 'brekfast'
				);
			}
			if ($kind == "dinner") {
				$mealDb = array(
					'dinner' => 'dinner'
				);
			}
//            LxRecipe::brekfast => LxRecipe::brekfast,
//            LxRecipe::dinner => LxRecipe::dinner,
//            LxRecipe::supper => LxRecipe::supper,
//            LxRecipe::snacks => LxRecipe::snacks,
//        );
//        
			foreach ($mealDb as $k => $v) {
				$mealsCurrent = array();
				$mealsCurrent = $this->getLxRecipeRepository()->getList($k)->getQuery()->getResult();
				$mealsData = array();
				foreach ($mealsCurrent as $value) {
					$mealsData[$value['p_id']]['name'] = $value['p_name'];

					$dir = 'uploads/documents/' . $value['d_path'];
					$base64 = base64_encode(file_get_contents(__DIR__ . '/../../../web/' . $dir));
					$mealsData[$value['p_id']]['img'] = $base64;
				}
				$mealDb[$k] = $mealsData;
			}

			$response = new Response(json_encode($mealDb));
		} catch (Exception $e) {
			$response = new Response(json_encode($e));
		}
		return $response;
	}

}
