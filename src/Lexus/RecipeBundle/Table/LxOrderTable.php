<?php

namespace Lexus\RecipeBundle\Table;

use Common\Core\LxTable;
use Doctrine\ORM\EntityManager;

class LxOrderTable extends LxTable {

	protected $tableName = "LxOrder";
	protected $bundleName = "LexusOrderBundle";
	protected $query = "getList";

	public function __construct($data) {
		$this->setData($data);
		$this->setRows();
		$this->setButtons();
		$this->setOptions();
	}
         
        public function setRows(){
   //     $tab[] = array('func' =>'','row' => 'p_id', 'title'=>'Id');
        $tab[] = array('func' =>'','row' => 'p_name', 'title'=>'Nazwa');
        $tab[] = array('func' =>'','row' => 'p_calories', 'title'=>'Kalorie');
        $tab[] = array('func' =>'','row' => 'p_protein', 'title'=>'Białka');
        $tab[] = array('func' =>'','row' => 'p_fat', 'title'=>'Tłuszcz');
        $tab[] = array('func' =>'','row' => 'p_carbohydrates', 'title'=>'Węglowodany');
        $tab[] = array('func' =>'','row' => 'p_sugars', 'title'=>'Cukier');
        $tab[] = array('func' =>'','row' => 'p_salt', 'title'=>'Sól');
        $tab[] = array('func' =>'','row' => 'p_group', 'title'=>'Grupa');
        
		$this->addRows($tab);
    }

	
	public function setButtons() {
		$tab[] = array('title' => 'Dodaj', 'routing' => 'lxproduct_add');
		$this->addButtons($tab);
	}

	private function filters() {
		
	}
}
