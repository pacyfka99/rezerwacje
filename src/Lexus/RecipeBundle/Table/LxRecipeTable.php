<?php

namespace Lexus\RecipeBundle\Table;

use Common\Core\LxTable;
use Doctrine\ORM\EntityManager;

class LxRecipeTable extends LxTable {

    protected $tableName = "LxRecipe";
    protected $bundleName = "LexusRecipeBundle";
    protected $query = "getList";

    public function __construct($data) {
        $this->setData($data);
        $this->setRows();
        $this->setButtons();
		$this->setOptions();
    }

    public function setRows() {
        $tab[] = array('func' => null, 'row' => 'p_id', 'title' => 'Id');
        $tab[] = array('func' => null, 'row' => 'p_name', 'title' => 'Nazwa');
        $tab[] = array('func' => 'showFile', 'row' => 'd_path', 'title' => 'Obraz');
		$this->addRows($tab);
    }

    public function setButtons() {
		$tab[] = array('title' => 'Dodaj', 'routing' => 'lxrecipe_add');
		$this->addButtons($tab);
    }

    private function setFilters() {
        
    }

    public function showFile($data) {
        $file = $data['d_path'];
        if ($file == null || $file == "") {
            return;
        }
        $dir = 'uploads/documents/' . $file;
        $base64 = base64_encode(file_get_contents(__DIR__ . '/../../../web/' . $dir));
        return '<img src="data:image/gif;base64,' . $base64 . '" alt="buźka" style="width:50px" />';
    }

}
