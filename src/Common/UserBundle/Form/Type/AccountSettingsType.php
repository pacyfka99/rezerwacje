<?php

namespace Common\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Common\UserBundle\Entity\User;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\CallbackTransformer;


class AccountSettingsType extends AbstractType {
    public function getName() {
        return 'accountSettings';
    }
	
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('username', \Symfony\Component\Form\Extension\Core\Type\TextType::class, array(
                    'label' => 'Telefon',
                    'required' => FALSE
                ));
		$builder->add('email', \Symfony\Component\Form\Extension\Core\Type\EmailType::class, array(
                    'label' => 'Email',
                    'required' => FALSE
                ));
		
		 $builder->add('roles', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class, array(
            'choices' => array_flip(User::getRoleArray()),
			'placeholder' => 'brak',
			 	  'multiple' => true,
        ));
//                ->add('avatarFile', 'file', array(
//                    'label' => 'Zmień avatar',
//                    'required' => FALSE
//                ))
        $save = 'Zapisz zmiany';

		$builder->add('save', SubmitType::class, array('label' => $save));
    }
    
    public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => User::class,
		));
	}

    
}