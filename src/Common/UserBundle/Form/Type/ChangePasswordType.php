<?php

namespace Common\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Common\UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class ChangePasswordType extends AbstractType {

	public function getName() {
		return 'changePassword';
	}

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
				->add('currentPassword', PasswordType::class, array(
					'label' => 'Aktualne hasło',
					'mapped' => false,
					'required' => FALSE,
                'constraints' => array(
                    new UserPassword(array(
                        'message' => 'Podano błędne aktualne hasło użytkownika'
                    ))
                )
		));
		$builder->add('plainPassword', \Symfony\Component\Form\Extension\Core\Type\RepeatedType::class, array(
					 'type' => PasswordType::class,
					'required' => FALSE,
					'first_options' => array(
						'label' => 'Nowe hasło'
					),
					'second_options' => array(
						'label' => 'Powtórz hasło'
					)
				))
				->add('submit', SubmitType::class, array(
					'label' => 'Zmień hasło'
		));
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => User::class,
			 'validation_groups' => array('Default', 'ChangePassword')
		));
	}

}
