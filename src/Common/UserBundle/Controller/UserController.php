<?php

namespace Common\UserBundle\Controller;

use Common\UserBundle\Form\Type\AccountSettingsType;
use Common\UserBundle\Exception\UserException;
use Common\UserBundle\Form\Type\ChangePasswordType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class UserController extends Controller {

	/**
	 * @Route(
	 *      "/account-settings/{id}",
	 *      name = "user_accountSettings"
	 * )
	 * @Security("is_granted('ROLE_SUPER_ADMIN')")
	 * @Template()
	 */
	public function accountSettingsAction(Request $Request, \Common\UserBundle\Entity\User $user) {
		$User = $this->getUser();
		// Account Settings Form
		$accountSettingsForm = $this->createForm(AccountSettingsType::class, $user);

		if ($Request->isMethod('POST') && $Request->request->has('account_settings')) {
			$accountSettingsForm->handleRequest($Request);

			if ($accountSettingsForm->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($user);
				$em->flush();

				$this->get('session')->getFlashBag()->add('success', 'Twoje dane zostały zmienione!');
				return $this->redirect($this->generateUrl('user_accountSettings', array('id' => $user->getId())));
			} else {
				$this->get('session')->getFlashBag()->add('error', 'Popraw błędy formularza!');
			}
		}


		// Change Password
		$changePasswdForm = $this->createForm(ChangePasswordType::class, $user);
		if ($Request->isMethod('POST') && $Request->request->has('change_password')) {
			$changePasswdForm->handleRequest($Request);

			if ($changePasswdForm->isValid()) {

				try {
					$userManager = $this->get('user_manager');
					$userManager->changePassword($user);

					$this->get('session')->getFlashBag()->add('success', 'Twoje hasło zostało zmienione!');
					return $this->redirect($this->generateUrl('user_accountSettings', array('id' => $user->getId())));
				} catch (UserException $ex) {
					$this->get('session')->getFlashBag()->add('error', $ex->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('error', 'Popraw błędy formularza2!');
			}
		}


		return array(
			'user' => $user,
			'accountSettingsForm' => $accountSettingsForm->createView(),
			'changePasswdForm' => $changePasswdForm->createView()
		);
	}

	/**
	 * @Route(
	 *      "/users-list/{page}", 
	 *      name="userslist",
	 *      requirements={"page"="\d+"},
	 *      defaults={"page"=1}
	 * )
	 * @Security("is_granted('ROLE_SUPER_ADMIN')")
	 * 
	 * @Template()
	 */
	public function usersListAction(Request $Request, $page) {

		$UserRepository = $this->getDoctrine()->getRepository('CommonUserBundle:User');

		$qb = $UserRepository->getQueryBuilder();

		//$Request = $this->getRequest();
		$limit = $Request->query->getInt('limit', $this->container->getParameter('pagination_limit'));

		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate($qb, $page, $limit);

		return array(
			'currPage' => 'users',
			'pagination' => $pagination
		);
	}

}
