<?php

namespace Common\UserBundle\Controller;

//use Symfony\Component\Security\Core\SecurityContextInterface;
//use Symfony\Component\Security\Core\Exception\AuthenticationException;


use Common\UserBundle\Entity\User;
use Common\UserBundle\Exception\UserException;
use Common\UserBundle\Form\Type\LoginType;
use Common\UserBundle\Form\Type\RegisterUserType;
use Common\UserBundle\Form\Type\RememberPasswordType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class LoginController extends Controller {

	/**
	 * @Route(
	 *      "/login",
	 *      name = "login"
	 * )
	 * 
	 * @Template()
	 */
	public function loginAction(Request $Request) {

		$authenticationUtils = $this->get('security.authentication_utils');

		// get the login error if there is one
		$error = $authenticationUtils->getLastAuthenticationError();

		// last username entered by the user
		$lastUsername = $authenticationUtils->getLastUsername();




		$Session = $this->get('session');

			$loginError = "Błędne dane logowania";
		// Login Form
//		if ($Request->attributes->has(Security::AUTHENTICATION_ERROR)) {
//			$loginError = $Request->attributes->get(Security::AUTHENTICATION_ERROR);
//			$loginError = "Błędne dane logowania";
//		} else {
//            $loginError = $Session->remove(Security::AUTHENTICATION_ERROR);
//		}

		if (isset($error)) {

//            $this->get('session')->getFlashBag()->add('error', $loginError->getMessage());
			$this->get('session')->getFlashBag()->add('danger', $loginError);
		}
		$loginForm = $this->createForm(LoginType::class, array(
			'username' => $Session->get(Security::LAST_USERNAME)
		));



//        // Remember Password Form
//        $rememberPasswdForm = $this->createForm(RememberPasswordType::class);
//        if ($Request->isMethod('POST')) {
//
//            $rememberPasswdForm->handleRequest($Request);
//
//            if ($rememberPasswdForm->isValid()) {
//                try {
//
//                    $userEmail = $rememberPasswdForm->get('email')->getData();
////                
//                    $userManager = $this->get('user_manager');
//                    $userManager->sendResetPasswordLink($userEmail);
//				
//                    $this->get('session')->getFlashBag()->add('success', 'Instrukcje resetowania hasĹ‚a zostaĹ‚y wysĹ‚ane na adres e-mail.');
//                    return $this->redirect($this->generateUrl('login'));
//                } catch (UserException $exc) {
//                    $error = new FormError($exc->getMessage());
//                    $rememberPasswdForm->get('email')->addError($error);
//                }
//            }
//        }
//        // Register User Form
//        $User = new User();
//
//        $registerUserForm = $this->createForm(RegisterUserType::class, $User);
//
//        if ($Request->isMethod('POST')) {
//            $registerUserForm->handleRequest($Request);
//			$User->setRoles(array('ROLE_USER'));
//            if ($registerUserForm->isValid()) {
//
//                try {
//
//                    $userManager = $this->get('user_manager');
//                    $userManager->registerUser($User);
//
//                    $this->get('session')->getFlashBag()->add('success', 'Konto zostało utworzone. Na TwojÄ… skrzynkÄ™ pocztowÄ… zostaĹ‚a wysĹ‚ana wiadomoĹ›Ä‡ aktywacyjna.');
//
//                    return $this->redirect($this->generateUrl('login'));
//                } catch (UserException $ex) {
//                    $this->get('session')->getFlashBag()->add('danger', $ex->getMessage());
//                }
//            }
//        }

		return array(
			'loginForm' => $loginForm->createView(),
//            'rememberPasswdForm' => $rememberPasswdForm->createView(),
//			'registerUserForm' => $registerUserForm->createView(),
			'error' => $error,
		);
	}

	/**
	 * @Route(
	 *      "/register",
	 *      name = "register"
	 * )
	 * 
	 * @Template()
	 */
	public function registerAction(Request $Request) {
		// Register User Form
		$User = new User();
		$User->setEnabled(true);
//		$error = $authenticationUtils->getLastAuthenticationError();
		$registerUserForm = $this->createForm(RegisterUserType::class, $User);

		if ($Request->isMethod('POST')) {
			$registerUserForm->handleRequest($Request);
			$User->setRoles(array('ROLE_USER'));
			if ($registerUserForm->isValid()) {

				try {

					$userManager = $this->get('user_manager');
					$userManager->registerUser($User);

					$this->get('session')->getFlashBag()->add('success', 'Konto zostało utworzone.');

					return $this->redirect($this->generateUrl('login'));
				} catch (UserException $ex) {
					$this->get('session')->getFlashBag()->add('danger', $ex->getMessage());
				}
			}
		}

		return array(
//			'loginForm' => $loginForm->createView(),
//            'rememberPasswdForm' => $rememberPasswdForm->createView(),
			'registerUserForm' => $registerUserForm->createView(),
//			'error' => $error,
		);
	}

	/**
	 * @Route(
	 *      "/reset-password/{actionToken}",
	 *      name = "user_resetPassword"
	 * )
	 */
	public function resetPasswordAction($actionToken) {
		try {

			$userManager = $this->get('user_manager');
			$userManager->resetPassword($actionToken);

			$this->get('session')->getFlashBag()->add('success', 'Na Twój adres e-mail zostało wysłane nowe hasło!');
		} catch (Exception $ex) {
			$this->get('session')->getFlashBag()->add('error', $ex->getMessage());
		}

		return $this->redirect($this->generateUrl('login'));
	}

	/**
	 * @Route(
	 *      "/account-activation/{actionToken}",
	 *      name = "user_activateAccount"
	 * )
	 */
	public function activateAccountAction($actionToken) {
		try {

			$userManager = $this->get('user_manager');
			$userManager->activateAccount($actionToken);

			$this->get('session')->getFlashBag()->add('success', 'Twoje konto zostało aktywowane!');
		} catch (UserException $ex) {
			$this->get('session')->getFlashBag()->add('error', $ex->getMessage());
		}

		return $this->redirect($this->generateUrl('login'));
	}

	/**
	 * @Route(
	 *      "/logout",
	 *      name = "logout")
	 */
	public function logoutAction() {

	}

}
