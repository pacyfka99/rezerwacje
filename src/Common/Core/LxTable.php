<?php

namespace Common\Core;

Class LxTable {

	protected $query;
	protected $tableName;
	protected $bundleName;
	protected $em;
	protected $doctrine;
	protected $paginator;
	protected $templating;
	protected $request;
	protected $rows;
	protected $buttons;
	protected $router;
	protected $options = array();

    protected function setData($data) {
        if (isset($data['entityManager']))
            $this->em = $data['entityManager'];
        if (isset($data['doctrine']))
            $this->doctrine = $data['doctrine'];
        if (isset($data['paginator']))
            $this->paginator = $data['paginator'];
        if (isset($data['templating']))
            $this->templating = $data['templating'];
        if (isset($data['request']))
            $this->request = $data['request'];
        if (isset($data['router']))
            $this->router = $data['router'];
        
        $this->request->getSession()->getFlashBag()->add('sukces','SZATAN');
       
    }
    public function addButtons($button){
        $this->buttons = $button;
    }
 

	protected function setOptions() {

		$tab[] = array('func' => '', 'row' => '', 'group' => "Opcje", 'title' => 'Edytuj', 'routing' => strtolower($this->tableName) . '_edit');
		$tab[] = array('func' => '', 'row' => '', 'group' => "Opcje", 'title' => 'Usuń', 'routing' => strtolower($this->tableName) . '_delete');

		$this->addOptions($tab);
	}

	public function addRows($row) {
		$this->rows = $row;
	}

	public function addOptions($row) {
		$this->options = $row;
	}

	private function getQuery() {
		$repository = $this->doctrine->getRepository($this->bundleName . ':' . $this->tableName);
		$query = $this->query;
		$rows = $repository->$query();

		$paginator = $this->paginator->paginate($rows, $this->request->get('page', 1), 3);
		return $paginator;
	}

	public function run() {

		$paginator = $this->getQuery();

		foreach ($this->options as $item) {
			$this->rows[] = $item;
		}

		foreach ($this->rows as $k => $row) {
			if ($row['func'] != null) {
				$paginItems = [];
				foreach ($paginator->getItems() as $item) {
					$data = $this->$row['func']($item);
					$item[$row['row']] = $data;
					$paginItems[] = $item;
				}
				$paginator->setItems($paginItems);
			}
			if (isset($row['group'])) {
					$this->rows[$row['group']][] = $row;
					unset($this->rows[$k]);
			}
		}
	
		$paginator = $this->templating->render('LexusRecipeBundle:Default:table.html.twig', [
			'pagination' => $paginator,
			'rows' => $this->rows,
			'buttons' => $this->buttons,
		]);

		return $paginator;
	}

	protected function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH) {
		return $this->router->generate($route, $parameters, $referenceType);
	}

}
