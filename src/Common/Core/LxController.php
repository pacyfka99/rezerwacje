<?php

namespace Common\Core;

use Lexus\RecipeBundle\Entity\LxRecipeRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LxController extends Controller {

    public function getData(){
        $data['entityManager'] = $this->getDoctrine()->getManager();
        $data['doctrine'] = $this->getDoctrine();
        $data['paginator'] = $this->get('knp_paginator');
        $data['templating'] = $this->container->get('templating');
        $data['request'] = $this->container->get('request_stack')->getCurrentRequest();
        $data['router'] = $this->container->get('router');
        return $data;
    }
    
    /** @var LxRecipeRepository */
    public function getLxRecipeRepository(){
        $em = $this->getDoctrine()->getManager();
        $return =  $em->getRepository('LexusRecipeBundle:LxRecipe');
        return $return;
    }
	
	/** @var LxProductRepository */
    public function getLxProductRepository(){
        $em = $this->getDoctrine()->getManager();
        $return =  $em->getRepository('LexusRecipeBundle:LxProduct');
        return $return;
    }
	/** @var LxRecipeProductRepository */
    public function getLxRecipeProductRepository(){
        $em = $this->getDoctrine()->getManager();
        $return =  $em->getRepository('LexusRecipeBundle:LxRecipeProduct');
        return $return;
    }
	
	/** @var LxRecipeDietRepository */
    public function getLxRecipeDietRepository(){
        $em = $this->getDoctrine()->getManager();
        $return =  $em->getRepository('LexusRecipeBundle:LxDiet');
        return $return;
    }
	/** @var LxRecipeBedRepository */
    public function getLxBedRepository(){
        $em = $this->getDoctrine()->getManager();
        $return =  $em->getRepository('LexusRecipeBundle:LxBed');
        return $return;
    }
	/** @var LxRecipeRoomRepository */
    public function getLxRoomRepository(){
        $em = $this->getDoctrine()->getManager();
        $return =  $em->getRepository('LexusRecipeBundle:LxRoom');
        return $return;
    }
	/** @var LxRecipeOrderRepository */
    public function getLxOrderRepository(){
        $em = $this->getDoctrine()->getManager();
        $return =  $em->getRepository('LexusRecipeBundle:LxOrder');
        return $return;
    }
	/** @var LxRecipeOrderItemRepository */
    public function getLxOrderItemRepository(){
        $em = $this->getDoctrine()->getManager();
        $return =  $em->getRepository('LexusRecipeBundle:LxOrderItem');
        return $return;
    }
	/** @var UsersRepository */
    public function getUsersRepository(){
        $em = $this->getDoctrine()->getManager();
        $return =  $em->getRepository('CommonUserBundle:User');
        return $return;
    }
}
